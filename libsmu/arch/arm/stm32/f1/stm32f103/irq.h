/*
 *  Copyright © 2022 Dylan Wadler dylan@slant.tech
 *
 *  This file is part of libsmu.
 *
 *  libsmu is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  libsmu is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libsmu.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


/* Enumerated IRQ Definitions */
#define IRQ_WWDG 0
#define IRQ_PVD 1
#define IRQ_TAMPER 2
#define IRQ_RTC 3
#define IRQ_FLASH 4
#define IRQ_RCC 5
#define IRQ_EXTI0 6
#define IRQ_EXTI1 7
#define IRQ_EXTI2 8
#define IRQ_EXTI3 9
#define IRQ_EXTI4 10
#define IRQ_DMA1_CHANNEL1 11
#define IRQ_DMA1_CHANNEL2 12
#define IRQ_DMA1_CHANNEL3 13
#define IRQ_DMA1_CHANNEL4 14
#define IRQ_DMA1_CHANNEL5 15
#define IRQ_DMA1_CHANNEL6 16
#define IRQ_DMA1_CHANNEL7 17
#define IRQ_ADC1_2 18
#define IRQ_USB_HP_CAN_TX 19
#define IRQ_USB_LP_CAN_RX0 20
#define IRQ_CAN_RX1 21
#define IRQ_CAN_SCE 22
#define IRQ_EXTI9_5 23
#define IRQ_TIM1_BRK 24
#define IRQ_TIM1_UP 25
#define IRQ_TIM1_TRG_COM 26
#define IRQ_TIM1_CC 27
#define IRQ_TIM2 28
#define IRQ_TIM3 29
#define IRQ_TIM4 30
#define IRQ_I2C1_EV 31
#define IRQ_I2C1_ER 32
#define IRQ_I2C2_EV 33
#define IRQ_I2C2_ER 34
#define IRQ_SPI1 35
#define IRQ_SPI2 36
#define IRQ_USART1 37
#define IRQ_USART2 38
#define IRQ_USART3 39
#define IRQ_EXTI15_10 40
#define IRQ_RTC_ALARM 41
#define IRQ_USB_WAKEUP 42
#define IRQ_TIM8_BRK 43
#define IRQ_TIM8_UP 44
#define IRQ_TIM8_TRG_COM 45
#define IRQ_TIM8_CC 46
#define IRQ_ADC3 47
#define IRQ_FSMC 48
#define IRQ_SDIO 49
#define IRQ_TIM5 50
#define IRQ_SPI3 51
#define IRQ_UART4 52
#define IRQ_UART5 53
#define IRQ_TIM6 54
#define IRQ_TIM7 55
#define IRQ_DMA2_CHANNEL1 56
#define IRQ_DMA2_CHANNEL2 57
#define IRQ_DMA2_CHANNEL3 58
#define IRQ_DMA2_CHANNEL4_5 59
#define IRQ_DMA2_CHANNEL5 60
#define IRQ_ETH 61
#define IRQ_ETH_WKUP 62
#define IRQ_CAN2_TX 63
#define IRQ_CAN2_RX0 64
#define IRQ_CAN2_RX1 65
#define IRQ_CAN2_SCE 66
#define IRQ_OTG_FS 67
#define IRQ_MAX_NUM 68

void wwdg(void);
void pvd(void);
void tamper(void);
void rtc(void);
void flash(void);
void rcc(void);
void exti0(void);
void exti1(void);
void exti2(void);
void exti3(void);
void exti4(void);
void dma1_channel1(void);
void dma1_channel2(void);
void dma1_channel3(void);
void dma1_channel4(void);
void dma1_channel5(void);
void dma1_channel6(void);
void dma1_channel7(void);
void adc1_2(void);
void usb_hp_can_tx(void);
void usb_lp_can_rx0(void);
void can_rx1(void);
void can_sce(void);
void exti9_5(void);
void tim1_brk(void);
void tim1_up(void);
void tim1_trg_com(void);
void tim1_cc(void);
void tim2(void);
void tim3(void);
void tim4(void);
void i2c1_ev(void);
void i2c1_er(void);
void i2c2_ev(void);
void i2c2_er(void);
void spi1(void);
void spi2(void);
void usart1(void);
void usart2(void);
void usart3(void);
void exti15_10(void);
void rtc_alarm(void);
void usb_wakeup(void);
void tim8_brk(void);
void tim8_up(void);
void tim8_trg_com(void);
void tim8_cc(void);
void adc3(void);
void fsmc(void);
void sdio(void);
void tim5(void);
void spi3(void);
void uart4(void);
void uart5(void);
void tim6(void);
void tim7(void);
void dma2_channel1(void);
void dma2_channel2(void);
void dma2_channel3(void);
void dma2_channel4_5(void);
void dma2_channel5(void);
void eth(void);
void eth_wkup(void);
void can2_tx(void);
void can2_rx0(void);
void can2_rx1(void);
void can2_sce(void);
void otg_fs(void);
