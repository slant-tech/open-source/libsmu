#ifndef LIBSMU_USART_H
#define LIBSMU_USART_H

#include <stdint.h>
#include <libsmu/utils/commondef.h>

/* Write single byte to usart peripheral */
int usart_write_byte( void * restrict usart, uint8_t byte );

/* Read single byte from usart peripheral */
uint8_t usart_read_byte( void * restrict usart );

/* Write multiple bytes to usart peripheral */
int usart_write( void * restrict usart, unsigned int len, uint8_t data[len]);

/* Read multiple bytes from usart peripheral. Only works if queueing is enabled */
#ifdef LIBSMUUSART_QUEUE_RX
int usart_read( void * restrict usart, unsigned int len, uint8_t * restrict data[len]);
#endif

/* Set baud rate */
int usart_set_baud( void * restrict usart, unsigned int baud );

#endif /* LIBSMU_USART_H */
