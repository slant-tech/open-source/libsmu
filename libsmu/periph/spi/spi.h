#ifndef LIBSMUSPI_H
#define LIBSMUSPI_H

#include <stdint.h>
#include <libsmu/utils/commondef.h>

/* Write single word to spi peripheral */
#ifdef LIBSMUSPI_8BIT
int spi_write_byte( void * restrict spi, uint8_t data );
#endif
#ifdef LIBSMUSPI_16BIT
int spi_write_byte( void * restrict spi, uint16_t data );
#endif

/* Read single word from spi peripheral */
#ifdef LIBSMUSPI_8BIT
uint8_t spi_read_byte( void * restrict spi );
#endif

#ifdef LIBSMUSPI_16BIT
uint16_t spi_read_byte( void * restrict spi );
#endif


#endif /* LIBSMUSPI_H */
