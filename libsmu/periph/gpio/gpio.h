/*
 *  Copyright © 2021 Dylan Wadler dylan@slant.tech
 *
 *  This file is part of libsmu.
 *
 *  libsmu is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  libsmu is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libsmu.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#ifndef LIBSMU_GPIO_H
#define LIBSMU_GPIO_H

#include <stdint.h>
#include <libsmu/utils/commondef.h>

#define GPIO_INPUT	1
#define GPIO_OUTPUT	0


/* Initialize GPIO. Implementation specific, void pointer for configuration
 * struct if needed  */
void gpio_init( void * cfg );

/* Configure the gpio direction for specified port */
void gpio_port_direction( void * restrict port, uint32_t pins );

/* Configure gpio direction for single pin */
void gpio_pin_direction( void * restrict port, uint32_t pin );

/* Set GPIO port */
void gpio_port_set( void * restrict port, uint32_t pins );

/* Clear GPIO port */
void gpio_port_clear( void * restrict port, uint32_t pins );

/* Set GPIO pin */
void gpio_pin_direction_set( void * restrict port, uint32_t pin );

/* Clear GPIO pin */
void gpio_pin_clear( void * restrict port, uint32_t pin );

/* Configure alternate function for GPIO port */
void gpio_port_alt_cfg( void * restrict port, uint32_t pins, void * restrict cfg );

/* Configure alternate function for GPIO pin */
void gpio_pin_alt_cfg( void * restrict port, uint32_t pin, void * restrict cfg );

#endif /* LIBSMU_GPIO_H */
