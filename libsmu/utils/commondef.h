#ifndef LIBSYSMU_COMMONDEF_H
#define LIBSYSMU_COMMONDEF_H

#include <stdint.h>

/* Useful macros */
#ifndef __weak
#define __weak		__attribute__((weak))
#endif

#define UNUSED(x) 	(void)(x)

/* Define Bit fields */
struct bf8 {
	unsigned int b0:1;
	unsigned int b1:1;
	unsigned int b2:1;
	unsigned int b3:1;
	unsigned int b4:1;
	unsigned int b5:1;
	unsigned int b6:1;
	unsigned int b7:1;
};

/* Byte union:
 * Access byte through entire byte or individual bits */
typedef union {

	uint8_t byte;
	struct bf8 bits;


} __byte_t;

/* Byte access for 16 bit word */
typedef union {
	uint8_t u8 [2];
	uint16_t u16;

} __barr_16_t;

/* Byte access for 32 bit word */
typedef union{
	uint8_t u8[4];
	uint32_t u32;
} __barr_32_t;

/* Byte access for 64 bit word */
typedef union{
	uint8_t u8[8];
	uint64_t u64;
} __barr_64_t;


/* Memory Mapped Address macros */
#define MEM8(x)			(*(volatile uint8_t *)(x))
#define MEM16(x)		(*(volatile uint16_t *)(x))
#define MEM32(x)		(*(volatile uint32_t *)(x))
#define MEM64(x)		(*(volatile uint64_t *)(x))

/* Bitfield masking macros */
#define BITSET(pos)			((1) << (pos))

/* Set register bits */
#define REGBITSET(r, val)	*(r) |= (val)

/* Clear register bits */
#define REGBITCLR(r, val)	*(r) &= (~(val))

/* Set register field with offset */
#define REGFIELDSET(r, val, pos)	*(r) |= ( (val) << pos )

/* Clear register field with offset */
#define REGFIELDCLR(r, val, pos)	*(r) &= ~( (val) << pos )


/* 8 Bit microcontroller specific definitions */
#ifdef LIBSYSMU_8BIT
#define 
#endif

/* 16 Bit microcontroller specific definitions */
#ifdef LIBSYSMU_16BIT
#define 
#endif

/* 32 Bit microcontroller specific definitions */
#ifdef LIBSYSMU_32BIT
#define 
#endif

#endif /* LIBSYSMU_COMMONDEF_H */
