# New MCU Request

## Description

Architecture:   [required] \
Manufacturer:   [required] \
Family:         [required] \
Part number:    [required] \
Datasheet:      [Link to datasheet](https://example.com) 

## Peripherals
- [ ] periph 1
- [ ] periph 2

## Related Issues
[optional; delete this line if none exist already]

## Notes
Notes for implementation [optional; delete this line if not used]


/label ~request
/label ~defined
