# Bug Report

## Summary
Description of what occurred

## Environment
Details about the environment

MCU: [replace]
Compiler: [replace]
OS: [replace]


## Steps to reproduce

1. Step 1
2. Step 2
3. Step 3

## Scope
What scope the bug affects

[example: maintainer scripts, peripheral driver, specific mcu driver, etc.]

/label ~bug
