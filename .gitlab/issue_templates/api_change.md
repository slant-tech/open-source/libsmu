# API Change Request

## Summary
What to change and why [required]

## Proposal

### Old
```c
void old_function( void );
```

### New
```c
void new_function( uint8_t data );
```
#### Parameters
Definitions for the new parameters

```
data:   byte for options 
```

### Reasoning behind change
[required]

## Documentation
Documenation to add/change to reflect changes [required]

## Files affected
[file](/path/in/repo)


/label ~request
