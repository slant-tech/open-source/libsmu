# New MCU

## Description

Architecture:   [required] \
Manufacturer:   [required] \
Family:         [required] \
Part number:    [required] \
Datasheet:      [Link to datasheet](https://example.com) 

## Target Milestone to include in
%v1.0.0

## Configuration Documentation
Wiki link for configuration of a project with this mcu


## Related Issues
#123+ [optional; delete if not used]

## Proposed Maintainers
- @user [email](mailto:email@example.com) [required]
- @user [email](mailto:email@example.com) [optional]
