# Maintainer Files

This directory contains all the microcontroller setup files to generate all the
necessary peripheral and memory mapped files. Essentially, the microcontroller
definiton.

It will not implement the functions, only provide skeleton .c files to then
implement the desired functions. Refer to the 'Contribution' file to learn more
about how to add a new microcontroller.

## Directory Structure

### maintainer
This directory contains JSON files detailing the different microcontrollers
to be used by the maintainer scripts to generate all required .c and .h files
for the library.

### mcu
This directory contains all relevant microcontrollers, segmented by their
architecture, then manufacturer, and then finally their family. For example,
the STM32F103 would be supported under the mcu/stm32/f1 directory. Given that
some microcontrollers within the same family may have different enabled
peripherals, they will contain their own folder. Peripherals may also differ in
functionality while having the same name across manufacturers or even within
the same family, but the API will remain the same. The core implementation will
determine what actually happens. Special features that only that
microcontroller supports will need to be implemented outside of the scripting

### scripts
This directory contains any additional scripts specific for maintaining the
library. By separating these scripts from the top directory, users of the
library should not accidentally use the maintainer scripts and cause issues to
their instance.
