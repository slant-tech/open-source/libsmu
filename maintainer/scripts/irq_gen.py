#!/usr/bin/python3

# This script reads the microcontroller definition file and generates the
# proper

import os
from os.path import exists
import sys
import json
from script_common import Mcu

project_license_file="license_header"

# Get dictionary from json irq array, print out c function for irq
def gen_irq_function_h( mcu, outfile ):
    f = open(outfile, "a")
    for irq in mcu.irq_vector:
        f.write("void " + irq + "(void);\n") 
    f.close()


# Generate irq weak vector .c file 
def gen_irq_function_c( mcu, outfile ):
    f = open(outfile, "a")
    for irq in mcu.irq_vector:
        f.write("__attribute__((weak)) void " + irq + "(void);\n")      
    f.close()

# Generate enumerated values for IRQs 
def gen_irq_enum(mcu, outfile):
    enum = 0
    f = open(outfile, "a")
    f.write("/* Enumerated IRQ Definitions */\n")
    for irq in mcu.irq_vector:
        f.write("#define IRQ_%s %d\n" % (irq.upper(), int(enum)))
        enum = enum + 1
    f.write("#define IRQ_MAX_NUM %d\n\n" % (int(enum)))
    f.close()


def gen_irq_h( mcu, outfile ):
    gen_irq_enum( mcu, outfile )
    gen_irq_function_h( mcu, outfile )


def main():

    micro = Mcu(sys.argv[1], project_license_file)

    # Print out micro info; useful for debugging only
    #micro.print_info()

    # Specify where to put files
    h_dir = sys.argv[2] + "/libsmu/arch/"
    c_dir = sys.argv[2] + "/lib/arch/"   

    # Generate irq files
    micro.gen_lib_files( c_dir, "irq.c", gen_irq_function_c, True)
    micro.gen_lib_files( h_dir, "irq.h", gen_irq_h, True)
    

if __name__ == "__main__":
    main()
