#!/usr/bin/python3

# Part class for mcu
import types
import os
from os.path import exists
import sys
import json
import re


class Mcu:
    pn      = ''                # Manufacturer base part number for microcontroller
    mfg     = ''                # Manufacturer name
    family  = ''                # microcontroller family group
    arch    = ''                # processor architecture / ISA
    bit     = ''                # Bit with for register sizes
    addr_sz = ''                # Size for variable pointer size
    license_file = ''           # source for license information
    peripherals = []            # list of peripherals supported
    irq_vector = []             # list of irq vectors

    # Json input to initialize class
    # json_file:        filename for mcu json defintion
    # license_file:     filename for license file
    def __init__(self, json_file, license_file):

        # parse json file 
        if( type(json_file) != str):
            raise TypeError("MCU JSON filename is not a string")

        # check that file exists 
        if (not exists(json_file) ):
            raise Exception('MCU JSON file does not exist: {}'.format(json))

        # parse json file to be used in python
        mcu_file = open(json_file, 'r')
        mcu_data = json.load(mcu_file)

        self.pn = mcu_data["mcu"]["info"]["pn"]
        self.mfg = mcu_data["mcu"]["info"]["mfg"]
        self.family = mcu_data["mcu"]["info"]["family"]
        self.arch = mcu_data["mcu"]["info"]["arch"]
        self.bit = mcu_data["mcu"]["info"]["bit"]
        self.addr_sz = mcu_data["mcu"]["info"]["addr_sz"]
        self.license_file = license_file
        self.peripherals = mcu_data["mcu"]["peripherals"].copy()
        self.irq_vector = mcu_data["mcu"]["irq_vector"].copy()
        

    # Write the license to the output file 
    def __write_license( self, outfile ):
        f = open(outfile, "w")
        lfile = open(self.license_file, "r")
        f.write( lfile.read() )
        f.write( "\n\n" )
        f.close()
        lfile.close()

    # Generate macro for address
    def __write_addr( self, name, addr, outfile ):
        macro = "#define " + name.toupper() + " (*(volatile uint" + str(self.addr_sz) + "_t *)( 0x" + hex(addr) + ")\n"
        f = open(outfile, "w")
        f.write( "{}\n".format(macro) )
        f.close()

    
    # Refactor existing function that matches 'old' (just function name),
    # replace with new (entire line required for function)
    def __refactor_function( file, old, new ):
        # check for strings
        if( type(old) != str):
            raise TypeError("Function to replace in refactor is not a string")
        
        if( type(new) != str):
            raise TypeError("Function to use as replacement in refactor is not a string")

        if( type(file) != str):
            raise TypeError("Filename in refactor is not a string")

        # create regex string for searching
        funct_regex = r'.*(?<=(?<=int\s)|(?<=void\s)|(?<=string\s)|(?<=double\s)|(?<=float\s)|(?<=char\s))[^=]*?' + old + r'(?=\s?\().+'

        # replace function name defintion; don't do anything with calls to that function
        [re.findall(funct_regex, line)
            for line in open(file)]

    # Determine type to use in generated file, from json type
    def __json_arg_type(self, jtype):

        if( type(jtype) != str ):
            raise TypeError("Name for library file is not a string")

        match jtype:
            # Type hard coded in file
            case ("void"|"uint32_t"|"uint16_t"|"uint8_t"|"int32_t"|"int16_t"|"int8_t"|"int"|"char"|"float"|"double"):
                return jtype
            # For pointers, make sure to use restrict keyword
            case ("void*"|"uint32_t*"|"uint16_t*"|"uint8_t*"|"int32_t*"|"int16_t*"|"int8_t*"|"int*"|"char*"|"float*"|"double*"):
                return jtype + "restrict"
            # Unsigned register size            
            case "ureg":
                return "uint" + str(self.bit) + "_t"
            # Signed register size
            case "reg":
                return "int" + str(self.bit) + "_t"
            # Pointer to address size
            case "addr":
                return "uint" + str(self.addr_sz) + "_t * restrict"
            # Pointer to unsigned register
            case "ureg_ptr":
                return "uint" + str(self.bit) + "_t * restrict" 
            # Pointer to signed register
            case "reg_ptr":
                return "int" + str(self.bit) + "_t * restrict" 
            # Default; throw error to make debugging weird type issues easier
            case _:
                raise Exception("Unrecognized argument type: `{}'".format(jtype))


    # Print out all information about microcontroller
    def print_info(self):
        print( '\n   --- MCU Information ---   ')
        print(r'Part number:   {}'.format(self.pn))
        print(r'Manufacturer:  {}'.format(self.mfg))
        print(r'Family:        {}'.format(self.mfg))
        print(r'Architecture:  {}'.format(self.arch))
        print(r'Register Size: {}'.format(self.bit))
        print(r'Address Size:  {}'.format(self.addr_sz))
        print(r'Supported Peripherals:')
        for periph in self.peripherals:
            print(r'  - {}'.format(periph))
        
        print( '\n   --- End MCU Information ---\n')


    # Generates files based off of provided name, with function passed in 'gen'
    def gen_lib_files(self, directory, name, gen, delete_flag):
        # Generate filenames for output files
        outdir = directory + self.arch + "/" + self.mfg + "/" + self.family + "/" + self.pn

        # check if gen is a function
        if (type(gen) != types.FunctionType):
            raise TypeError("Function `{}' used to populate file is not a function, but type {}".format(gen, type(gen) ))

        # check if comp is a string
        if( type(name) != str ):
            raise TypeError("Name for library file is not a string")

        # create directory if it doesn't exist
        if (not exists(outdir) ):
            os.makedirs(outdir)

        outfile = outdir + "/" + name

        # Useful for debugging
        #print("File to write out: {}".format(outfile))

        # Refactor old files, unless delete flag is set 
        if( exists(outfile) and not(delete_flag) ):
            # Useful for debugging
            #print("Refactoring old version of {}\n".format(outfile))
            __refactor_function(outfile, "FIX")

        elif( exists(outfile) ):
            # Useful for debugging
            #print("Removing old version of {}\n".format(outfile))
            os.remove(outfile)



        # Writing out file
        self.__write_license( outfile )

        # call function
        gen( self, outfile )




