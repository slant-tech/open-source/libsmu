#!/bin/bash

# This file contains functions related to looking up the microcontroller's 
# parameters for the configuration of the build environment 


# Returns the microcontrollers architecture 
mcu_lookup_arch(){
	local mcu=$1

	# Return value 
	local arch

	case $mcu in

		# ARM Microcontrollers
		stm32*) ;&
		atsam*) ;&
		arm-generic)
			arch="arm"
			;;

		# AVR Microcontrollers 
		atmega*) ;&		# ATMEGA
		attiny*) ;&		# ATTINY
		avr*da*) ;&		# AVR DA
		avr*db*) ;&		# AVR DB
		avr*dd*) ;&		# AVR DD
		avr*ea*) ;&		# AVR EA
		avr-generic)
			arch="avr"
			;;

		# 16 Bit AVR Microcontrollers 
		atxmega*a*) ;&	# ATxmega A1U, A3U, A4U
		atxmega*b*) ;&	# ATxmega B1, B3
		atxmega*c*) ;&	# ATxmega C3, C4
		atxmega*d*) ;&	# ATxmega D3, D4
		atxmega*e*) ;&	# ATxmega E5
		atxmega-generic)
				arch="avr"
				;;

		# PIC Microcontrollers
		pic10*) ;&		# PIC10
		pic12*) ;&		# PIC12
		pic16*) ;&		# PIC16
		pic18*) ;&		# PIC18
		pic-generic)
				arch="PIC"
				;;

		# 16 Bit PIC Microcontrollers
		pic24f*) ;&		# PIC24F
		dispic33*) ;&	# disPIC33
		16pic-generic)
				arch="PIC16"
				;;

		# 32 Bit PIC Microcontrollers
		pic32mz*) ;&
		pic32mk*) ;&
		pic32mz*) ;&
		pic32cm*) ;&
		pic32mm*) ;&
		pic32-generic)
				arch="PIC32"
				;;

		# 32 Bit RISC-V Microcontrollers
		fe310*) ;&		# SiFive Freedom E310
		picorv32*) ;&	# PicoRV32 soft-core
		esp32-c3*) ;&	# Espressif ESP32-C3
		gd32*) ;&		# Gigiadevices RISC-V
		riscv-generic)
				arch="riscv32"
				;;

	esac

	#return relevant architecture setting 
	echo $arch

}

# Returns the architecture's usual toolchain prefix 
mcu_lookup_toolprefix(){
	local mcu=$1

	# Return value 
	local prefix

	case $mcu in

		# ARM Microcontrollers
		stm32*) ;&
		atsam*) ;&
		arm-generic)
			prefix="arm-none-eabi"
			;;

		# AVR Microcontrollers 
		atmega*) ;&		# ATMEGA
		attiny*) ;&		# ATTINY
		avr*da*) ;&		# AVR DA
		avr*db*) ;&		# AVR DB
		avr*dd*) ;&		# AVR DD
		avr*ea*) ;&		# AVR EA
		avr-generic)
			prefix="avr"
			;;

		# 16 Bit AVR Microcontrollers 
		atxmega*a*) ;&	# ATxmega A1U, A3U, A4U
		atxmega*b*) ;&	# ATxmega B1, B3
		atxmega*c*) ;&	# ATxmega C3, C4
		atxmega*d*) ;&	# ATxmega D3, D4
		atxmega*e*) ;&	# ATxmega E5
		atxmega-generic)
				prefix="avr"
				;;

		# PIC Microcontrollers
		pic10*) ;&		# PIC10
		pic12*) ;&		# PIC12
		pic16*) ;&		# PIC16
		pic18*) ;&		# PIC18
		pic-generic)
				prefix="xc8"
				;;

		# 16 Bit PIC Microcontrollers
		pic24f*) ;&		# PIC24F
		dispic33*) ;&	# disPIC33
		16pic-generic)
				prefix="xc16"
				;;

		# 32 Bit PIC Microcontrollers
		pic32mz*) ;&
		pic32mk*) ;&
		pic32mz*) ;&
		pic32cm*) ;&
		pic32mm*) ;&
		pic32-generic)
				prefix="xc32"
				;;

		# 32 Bit RISC-V Microcontrollers
		fe310*) ;&		# SiFive Freedom E310
		picorv32*) ;&	# PicoRV32 soft-core
		esp32-c3*) ;&	# Espressif ESP32-C3
		gd32*) ;&		# Gigiadevices RISC-V
		riscv-generic)
				prefix="riscv32-unknown-elf"
				;;

	esac

	#return relevant architecture setting 
	echo $prefix

}


# Returns the microcontroller library target
mcu_lookup_target(){
	local mcu=$1

	# Return value 
	local target

	case $mcu in

		# ARM Microcontrollers

		# STM32
		stm32f0*)
			target="stm32f0"
		  ;;
		stm32f1*)
			target="stm32f1"
		  ;;
  		stm32f2*)
			target="stm32f2"
		  ;;
		stm32f3*)
			target="stm32f3"
		  ;;
		stm32f4*)
			target="stm32f4"
		  ;;
		stm32f7*)
			target="stm32f7"
		  ;;
		stm32l0*)
			target="stm32l0"
		  ;;
		stm32l1*)
			target="stm32l1"
		  ;;
		stm32l4*)
			target="stm32l4"
		  ;;
		stm32l5*)
			target="stm32l5"
		  ;;
		stm32g0*)
			target="stm32g0"
		  ;;
		stm32g4*)
			target="stm32g4"
		  ;;
		stm32u5*)
			target="stm32u5"
		  ;;
		stm32h7*)
			target="stm32h7"
		  ;;
		stm32wl*)
			target="stm32wl"
		  ;;
		stm32wb*)
			target="stm32wb"
		  ;;

		# ATSAM 
		atsaml1*)
			target="atsaml1X"
		  ;;
		atsaml2*)
			target="atsaml2X"
		  ;;
		atsamc*)
			target="atsamc"
		  ;;
		atsamd5*)
			target="atsamd5X"
		  ;;
		atsamd*)
			target="atsamd"
		  ;;
		atsam4*)
			target="atsam4"
		  ;;
		atsamg*)
			target="atsamg"
		  ;;
		atsame5*)
			target="atsame5X"
		  ;;
		atsams7*)
			target="atsams7X"
		  ;;
		atsame7*)
			target="atsame7X"
		  ;;
		atsamv7*)
			target="atsamv7X"
		  ;;

		# AVR Microcontrollers 

		# AVR Architecture information was taken from "GNU Toolchain for Atmel
		# AVR8 Embedded Processors" document
		# Link: https://ww1.microchip.com/downloads/en/DeviceDoc/avr8-gnu-toolchain-3.5.4.1709-readme.pdf

		# avr2
		at90s2313*) ;&
		at90s2323*) ;&
		at90s2333*) ;&
		at90s2343*) ;&
		attiny22*) ;&
		attiny26*) ;&
		at90s4414*) ;&
		at90s4433*) ;&
		at90s4434*) ;&
		at90s8515*) ;&
		at90c8534*) ;&
		at90s8535*)
			target="avr2"
		  ;;

		# avr25
		ata5272*) ;&
		ata6616c*) ;&
		attiny13*) ;&
		attiny2313*) ;&
		attiny24*) ;&
		attiny4313*) ;&
		attiny44*) ;&
		attiny84*) ;&
		attiny25*) ;&
		attiny45*) ;&
		attiny85*) ;&
		attiny261*) ;&
		attiny461*) ;&
		attiny861*) ;&
		attiny43u*) ;&
		attiny87*) ;&
		attiny48*) ;&
		attiny88*) ;&
		attiny828*) ;&
		attiny841*) ;&
		at86f401*)
			target="avr25"
		  ;;

		# avr3
		at43usb355*) ;&
		at76c711*)
			target="avr3"
		  ;;

		# avr31
		atmega103*) ;&
		at76c711*)
			target="avr31"
		  ;;

		# avr35
		ata5505*) ;&
		ata6617c*) ;&
		ata664251*) ;&
		at90usb82*) ;&
		at90usb162*) ;&
		atmega8u2*) ;&
		atmega16u2*) ;&
		atmega32u2*) ;&
		attiny167*) ;&
		attiny1634*)
			target="avr35"
		  ;;

		# avr4
		ata6285*) ;&
		ata6286*) ;&
		ata6289*) ;&
		ata6612c*) ;&
		atmega8*) ;&
		atmega48*) ;&
		atmega88*) ;&
		atmega8515*) ;&
		atmega8535*) ;&
		at90pwm1*) ;&
		at90pwm2*) ;&
		at90pwm3*) ;&
		at90pwm81*)
			target="avr4"
		  ;;

		# avr5
		atmega328*)
			target="avr5"
		  ;;

		# avr51

		# avr6

		# avrxmega2

		# avrxmega4

		# avrxmega5

		# avrxmega6

		# avrxmega7

		# avrtiny

		# avr1

		# avr10
		attiny40*) ;&
		attniy20*) ;&
		attiny10*) ;&
		attiny9*) ;&
		attiny5*) ;&
		attiny4*)
			target="avr10"
		  ;;

  		avr*da*)
			target="avrda"
		  ;;
		avr*db*)
			target="avrdb"
		  ;;
		avr*dd*)
			target="avrdd"
		  ;;
		avr*ea*)
			target="avrea"
		  ;;

		# PIC Microcontrollers
		pic10*) ;&		# PIC10
		pic12*) ;&		# PIC12
		pic16*) ;&		# PIC16
		pic18*) ;&		# PIC18

		# 16 Bit PIC Microcontrollers
		pic24f*) ;&		# PIC24F
		dispic33*) ;&	# disPIC33

		# 32 Bit PIC Microcontrollers
		pic32mz*) ;&
		pic32mk*) ;&
		pic32mz*) ;&
		pic32cm*) ;&
		pic32mm*)
			target="placeholder"
		;;

		# 32 Bit RISC-V Microcontrollers
		# SiFive Freedom E310
		fe310*)
			target="fe310"
		  ;;
		picorv32*) ;&	# PicoRV32 soft-core
		esp32-c3*) ;&	# Espressif ESP32-C3
		gd32*) ;&		# Gigiadevices RISC-V
		riscv-generic)
			target="riscv"
	  	  ;;

	esac

	#return relevant architecture setting 
	echo $target

}



