/*
 *  Copyright © 2021 Dylan Wadler dylan@slant.tech
 *
 *  This file is part of libsmu.
 *
 *  libsmu is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  libsmu is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libsmu.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


/* Configuration file to determine settings for compilation */

#ifndef LIBSMU_CONFIG_DEF_H
#define LIBSMU_CONFIG_DEF_H

/*** Peripheral Settings ***/

/* USART */
#define LIBSMU_USART_8BIT
#define LIBSMU_USART_16BIT

/* Enables queueing on Recieve */
#define LIBSMU_USART_QUEUE_RX

/* Enable queueing on Transmit */

/* SPI */
#define LIBSMU_SPI_8BIT
#define LIBSMU_SPI_16BIT


#endif
