# libsμ



## What is libsμ?
libsμ (libsmu) is a LGPLv3 licensed microcontroller abstraction layer library. The goal is to provide a simple but complete library for microcontroller functions with a constent usage.

## Supported Microcontrollers

The table below shows the current status of supported microcontrollers.
Status meaning:
- Stable: Release number has been given and is known to be functional
- In Progress: Active development, not stable
- Planned: Support is preliminary, microcontroller is not in code base at this time.

|  Architecture |  Manufacturer  | Family | Status | 
| ------------ | ------------ | ------------ |  ------------ |
| ARM  |  STMicro  | stm32f4 | In Progress |
| AVR  | Microchip  | atmegaX8P | In Progress |
| RISC-V  |  SiFive | FE310 | Planned |
| RISC-V  |   | PicoRV32 | Planned |
| RISC-V  | Espressif | ESP32-C3 | Planned |
| PIC | Microchip | PIC16F152XX | Planned |



