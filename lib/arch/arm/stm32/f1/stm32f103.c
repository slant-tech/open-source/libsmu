/*
 *  Copyright © 2021 Dylan Wadler dylan@slant.tech
 *
 *  This file is part of libsmu.
 *
 *  libsmu is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  libsmu is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libsmu.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


__attribute__((weak)) void wwdg(void);
__attribute__((weak)) void pvd(void);
__attribute__((weak)) void tamper(void);
__attribute__((weak)) void rtc(void);
__attribute__((weak)) void flash(void);
__attribute__((weak)) void rcc(void);
__attribute__((weak)) void exti0(void);
__attribute__((weak)) void exti1(void);
__attribute__((weak)) void exti2(void);
__attribute__((weak)) void exti3(void);
__attribute__((weak)) void exti4(void);
__attribute__((weak)) void dma1_channel1(void);
__attribute__((weak)) void dma1_channel2(void);
__attribute__((weak)) void dma1_channel3(void);
__attribute__((weak)) void dma1_channel4(void);
__attribute__((weak)) void dma1_channel5(void);
__attribute__((weak)) void dma1_channel6(void);
__attribute__((weak)) void dma1_channel7(void);
__attribute__((weak)) void adc1_2(void);
__attribute__((weak)) void usb_hp_can_tx(void);
__attribute__((weak)) void usb_lp_can_rx0(void);
__attribute__((weak)) void can_rx1(void);
__attribute__((weak)) void can_sce(void);
__attribute__((weak)) void exti9_5(void);
__attribute__((weak)) void tim1_brk(void);
__attribute__((weak)) void tim1_up(void);
__attribute__((weak)) void tim1_trg_com(void);
__attribute__((weak)) void tim1_cc(void);
__attribute__((weak)) void tim2(void);
__attribute__((weak)) void tim3(void);
__attribute__((weak)) void tim4(void);
__attribute__((weak)) void i2c1_ev(void);
__attribute__((weak)) void i2c1_er(void);
__attribute__((weak)) void i2c2_ev(void);
__attribute__((weak)) void i2c2_er(void);
__attribute__((weak)) void spi1(void);
__attribute__((weak)) void spi2(void);
__attribute__((weak)) void usart1(void);
__attribute__((weak)) void usart2(void);
__attribute__((weak)) void usart3(void);
__attribute__((weak)) void exti15_10(void);
__attribute__((weak)) void rtc_alarm(void);
__attribute__((weak)) void usb_wakeup(void);
__attribute__((weak)) void tim8_brk(void);
__attribute__((weak)) void tim8_up(void);
__attribute__((weak)) void tim8_trg_com(void);
__attribute__((weak)) void tim8_cc(void);
__attribute__((weak)) void adc3(void);
__attribute__((weak)) void fsmc(void);
__attribute__((weak)) void sdio(void);
__attribute__((weak)) void tim5(void);
__attribute__((weak)) void spi3(void);
__attribute__((weak)) void uart4(void);
__attribute__((weak)) void uart5(void);
__attribute__((weak)) void tim6(void);
__attribute__((weak)) void tim7(void);
__attribute__((weak)) void dma2_channel1(void);
__attribute__((weak)) void dma2_channel2(void);
__attribute__((weak)) void dma2_channel3(void);
__attribute__((weak)) void dma2_channel4_5(void);
__attribute__((weak)) void dma2_channel5(void);
__attribute__((weak)) void eth(void);
__attribute__((weak)) void eth_wkup(void);
__attribute__((weak)) void can2_tx(void);
__attribute__((weak)) void can2_rx0(void);
__attribute__((weak)) void can2_rx1(void);
__attribute__((weak)) void can2_sce(void);
__attribute__((weak)) void otg_fs(void);
