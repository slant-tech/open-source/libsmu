/*
 *  Copyright © 2021 Dylan Wadler dylan@slant.tech
 *
 *  This file is part of libsmu.
 *
 *  libsmu is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  libsmu is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libsmu.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#include <libsmu/periph/usart/usart.h>
#include <libsmu/utils/commondef.h>
#include <stdint.h>

/* Write a single byte to usart peripheral */
__attribute__((weak)) int usart_write_byte( void * restrict usart, uint8_t byte ){

	UNUSED( usart );
	UNUSED( byte );
	
	return 0;

}

/* Read single byte from usart peripheral */
__attribute__((weak)) uint8_t usart_read_byte( void * restrict usart ) {
	UNUSED( usart );
	
	return 0;
}

