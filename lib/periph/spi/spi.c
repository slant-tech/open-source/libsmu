/*
 *  Copyright © 2021 Dylan Wadler dylan@slant.tech
 *
 *  This file is part of libsmu.
 *
 *  libsmu is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  libsmu is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with libsmu.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <libsmu/periph/spi/spi.h>
#include <libsmu/utils/commondef.h>

__weak int spi_write_byte( void * restrict spi, uint8_t data ){
	UNUSED( spi );
	UNUSED( data );
	
	return 0;

}

#ifdef LIBSMUSPI_8BIT
__weak spi_read_byte( void * restrict spi, uint8_t data ){
	UNUSED( spi );
	UNUSED( data );
	
	return 0;

}
#endif

#ifdef LIBSMUSPI_16BIT
__weak spi_read_byte( void * restrict spi, uint8_t data ){
	UNUSED( spi );
	UNUSED( data );
	
	return 0;

}
#endif
